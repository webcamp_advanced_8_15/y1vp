<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Task".
 *
 * @property integer $taskId
 * @property string $name
 * @property string $detailes
 * @property string $created
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'detailes', 'created'], 'required'],
            [['detailes'], 'string'],
            [['created'], 'safe'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskId' => Yii::t('app', 'Task ID'),
            'name' => Yii::t('app', 'Name'),
            'detailes' => Yii::t('app', 'Detailes'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
}
