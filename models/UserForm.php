<?php
namespace app\models;

use yii\base\Model;

class UserForm extends Model
{
    public $firstName;
    public $lastName;
    public $email;

    public function rules()
    {
        return [
            [['firstName', 'lastName'], 'required'],
            ['email', 'email'],
        ];
    }

public function getFullName()
    {
        return $this->firstName.' '.$this->lastName;
    }
}

